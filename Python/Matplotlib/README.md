# Matplotlib

Matplotlib is a 2-D plotting library that helps in visualizing figures. Matplotlib emulates Matlab like graphs and visualizations. Matlab is not free, is difficult to scale and as a programming language is tedious. So, matplotlib in Python is used as it is a robust, free and easy library for data visualization.

![<https://miro.medium.com/max/1198/1*G3GHcehkrWy8xVRlvQRUQg.png>](https://miro.medium.com/max/1198/1*G3GHcehkrWy8xVRlvQRUQg.png)

The figure contains the overall window where plotting happens, contained within the figure are where actual graphs are plotted. Every Axes has an x-axis and y-axis for plotting. And contained within the axes are titles, ticks, labels associated with each axis. An essential figure of matplotlib is that we can more than axes in a figure which helps in building multiple plots, as shown below. In matplotlib, pyplot is used to create figures and change the characteristics of figures.

In this repository:

- Matplotlib_Basic_Plotting: Syntax for generic matplotlib plots
- Matplotlib_Layouts: Syntax for matplotlib subplots and paired axes.
