# Plotly

plotly.py is an interactive, open-source, and browser-based graphing library for Python.

Built on top of plotly.js, plotly.py is a high-level, declarative charting library. plotly.js ships with over 30 chart types, including scientific charts, 3D graphs, statistical charts, SVG maps, financial charts, and more.

Cufflinks binds Plotly directly to pandas dataframes.

Installation:

`pip install plotly`

`pip install chart_studio`

`pip install cufflinks`