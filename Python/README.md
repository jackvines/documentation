# Documentation
This is a space for collected notebook documentation on various Python libraries.

## Pandas
Pandas is an open source, BSD-licensed library providing high-performance, easy-to-use data structures and data analysis tools for the Python programming language.

Installation:

`pip install pandas`

## Matplotlib
Matplotlib is a Python 2D plotting library which produces publication quality figures in a variety of hard copy formats and interactive environments across platforms

Installation:

`pip install matplotlib`

## Seaborn

Seaborn is a Python data visualization library based on matplotlib. It provides a high-level interface for drawing attractive and informative statistical graphics.

Installation:

`pip install seaborn`

## SciPy
SciPy is a widely used ecosystem of open-source software for mathematics, science, and engineering.

Installation:

`pip install scipy`